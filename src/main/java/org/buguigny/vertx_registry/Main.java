// Project skeleton generated with vep.
// vep v0.2.1 (2020.03.21) - See https://gitlab.com/mszmurlo/vep - MIT License

package org.buguigny.vertx_registry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.core.http.HttpServer;
import io.vertx.core.http.HttpServerResponse;


import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;

 
  public class Main {

    Vertx vertx = null;


  private static final Logger log = LoggerFactory.getLogger(Main.class);

  public static void main(String[] args) {
    (new Main()).startup(args);
  }

  private void startup(String[] args) {
     VertxOptions opts = new VertxOptions()
     // add some options with setXXX() methods
     ;
     vertx = Vertx.vertx(opts);

    log.info("Starting HTTP server");

    HttpServer server = vertx.createHttpServer();  // Create the server
    Router router = Router.router(vertx);          // Define the router
    router.get("/:p").handler(this::rootHandler);  // 'GET /xxx' will be by the 'rootHandler' method
    router.route().handler(this::defHandler);      // Any other URL will be handeled by 'defHandler'
    server
      .requestHandler(router::accept)
      .listen(8080, ar -> {
        if(ar.succeeded()) {
          log.info("Server running on port 8080");
          
        }
        else {
          log.error("Could not start server on port 8080");
          System.exit(-1);
        }
      });
  }

  private void rootHandler(RoutingContext rc) {
    String parameter = rc.request().getParam("p");
    log.info("Got a request with parameter '{}'", parameter);
    rc.response()
      .putHeader("Content-Type", "text/text; charset=utf-8")
      .setStatusCode(200)
      .end("Sending back parameter '"+parameter+"'\n");
  }

  private void defHandler(RoutingContext rc) {
    log.warn("Got a request '{} {}'. Not permitted.", rc.request().method(), rc.request().absoluteURI());
    rc.response()
      .setStatusCode(404)
      .end();
  }
}
