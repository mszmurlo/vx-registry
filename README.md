<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
    <style>
    body {
      background-color: #F0F0F0;
      color: #202020;
      margin-right : 10%;
      margin-left : 10%;
      padding : 0px;
      font-size : 12pt; 
      font-family: Georgia, Palatino, Times, 'Times New Roman', sans-serif;
      background-position : top left;
      background-repeat : no-repeat;
      text-align : justify;
    }
    </style>
</head>
<body>

# `vx-registry`

The is the README.md file for the project "vx-registry".

------

<small>
Initialized on 2020/03/21 with 
`groovy vep.groovy -pn vx-registry -pkg org.buguigny.vertx_registry -mfunc`

vep v0.2.1 (2020.03.21) - See https://gitlab.com/mszmurlo/vep - MIT License
</small>
</body></html>
